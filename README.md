# Gradle script plugins

## Usage

Add to your `build.gradle`

```
buildscript {
    apply from: 'https://bitbucket.org/sycyhy/gradle-scripts/raw/master/<name>.deps.gradle', to: buildscript
}

apply from: 'https://bitbucket.org/sycyhy/gradle-scripts/raw/master/<name>.gradle'
```